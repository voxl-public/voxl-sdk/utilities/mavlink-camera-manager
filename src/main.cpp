/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <mutex>
#include <list>
#include <condition_variable>

#include <modal_journal.h>
#include <modal_start_stop.h>
#include <modal_pipe.h>
#include <voxl_cutils.h>

#include <c_library_v2/common/mavlink.h>
#include <c_library_v2/development/development.h>

#include "context.h"
#include "mavcam_manager.h"
#include "config_file.h"

#define PROCESS_NAME		"voxl-mavcam-manager"

static void print_help()
{
    printf("Usage: mavlink-camera-manager <options>\n");
    printf("Options:\n");
    printf("-c   load the config file only (used for scripted configuration)\n");
    printf("-h   print this help message\n");
    printf("-d   Enable debug messages\n");
    printf("-r   RTSP URI\n");
    printf("-n   camera name sent to GCS (default Camera0)\n");
    printf("\n");
    printf("camera name is NOT the MPA pipe name, that's set in the config file\n");
    printf("\n");
    printf("example of running the service by manually setting the RTSP URI:\n");
    printf("voxl-mavcam-manager -r rtsp://192.168.0.22:8900/live\n");
    printf("\n");
}


int main(int argc, char *argv[])
{
    int extra_options = 0;
    int command_line_option = 0;

    bool url_set{false};
    char url_name[RTSP_URI_MAX_SIZE];

    bool name_set{false};
    char cam_name[CAMERA_NAME_MAX_SIZE];

    bool debug_set{false};

    while ((command_line_option = getopt(argc, argv, "chdr:n:")) != -1) {
        switch (command_line_option) {
        case 'c':
            return config_file_read();
        case 'h':
            print_help();
            return 0;
        case 'd':
            //init_arr[i].context.debug = 1;
            debug_set = true;
            break;
        case 'r':
            printf("Setting Manual RTSP server URL: %s\n", optarg);
            strncpy(url_name, optarg, RTSP_URI_MAX_SIZE);
            //init_arr[0].context.rtsp_manual_uri = 1;
            url_set = true;
            break;
        case 'n': // TODO this should be an integer number 0-6
            printf("Camera name: %s\n", optarg);
            strncpy(cam_name, optarg, CAMERA_NAME_MAX_SIZE);
            name_set = true;
            break;
        case ':':
            fprintf(stderr, "Error: Option needs a value\n");
            print_help();
            return 0;
        case '?':
            printf("unknown option: %c\n", optopt);
            print_help();
            return 0;
        }
    }

    for (; optind < argc; optind++) {
        fprintf(stderr, "Error: Extra argument: %s\n", argv[optind]);
        extra_options = 1;
    }

    if (extra_options) {
        print_help();
        return PROGRAM_ERROR;
    }

    if(config_file_read()){
        return PROGRAM_ERROR;
    }
    config_file_print();

    // make sure another instance isn't running
    // if return value is -3 then a background process is running with
    // higher privaledges and we couldn't kill it, in which case we should
    // not continue or there may be hardware conflicts. If it returned -4
    // then there was an invalid argument that needs to be fixed.
    if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

    // start signal manager so we can exit cleanly
    if(enable_signal_handler()==-1){
        fprintf(stderr,"ERROR: failed to start signal manager\n");
        return -1;
    }

    // make PID file to indicate your project is running
    // due to the check made on the call to rc_kill_existing_process() above
    // we can be fairly confident there is no PID file already and we can
    // make our own safely.
    make_pid_file(PROCESS_NAME);
    MavCamManager init_arr[n_mavcam_inputs];

    for(int i=0; i<n_mavcam_inputs; i++){

        if(debug_set){
            init_arr[i].context.debug = 1;
        }

        if(url_set && i == 0){
            strcpy(init_arr[i].context.rtsp_server_uri, url_name);
            init_arr[i].context.rtsp_manual_uri = 1;
        } else {
            strcpy(init_arr[i].context.rtsp_server_uri, mavcam_inputs[i].default_uri);
            init_arr[i].context.rtsp_manual_uri = 0;
        }

        init_arr[i].context.enable_auto_ip = mavcam_inputs[i].enable_auto_ip;
        strcpy(init_arr[i].context.snapshot_pipe_name, mavcam_inputs[i].snapshot_pipe_name);
        strcpy(init_arr[i].context.video_pipe_name, mavcam_inputs[i].video_record_pipe_name);

        if(name_set && i == 0){
            strcpy(init_arr[i].context.camera_name, cam_name);
        } else {
            strncpy(init_arr[i].context.camera_name, mavcam_inputs[i].video_record_pipe_name, sizeof(init_arr[i].context.camera_name));
        }

        init_arr[i].stream_id = i + 1;
        init_arr[i].mavlink_system_id = mavcam_inputs[i].mavlink_sys_id;
        init_arr[i].context.compid = MAV_COMP_ID_CAMERA;
        init_arr[i].context.vid_pipe_ch = pipe_client_get_next_available_channel();
        init_arr[i].context.from_gcs_pipe_ch = pipe_client_get_next_available_channel();
        init_arr[i].context.to_gcs_pipe_ch = pipe_client_get_next_available_channel();

        printf("COMPID: %d\n", init_arr[i].context.compid);
        init_arr[i].qgc_init();
    }

    main_running = 1;
    // Idle loop. The real work is done in the QGC module threads.   
    while (main_running) {
        usleep(5000000);
    }

    for(int i=0; i<n_mavcam_inputs; i++){
        init_arr[i].qgc_shutdown();
    }
    remove_pid_file(PROCESS_NAME);
    pipe_client_close_all();

    return 0;
}
