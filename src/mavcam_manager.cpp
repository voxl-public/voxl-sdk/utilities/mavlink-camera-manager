/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "mavcam_manager.h"
#include "config_file.h"

void _gcs_connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    printf("Mavlink server Connected\n");
    return;
}

// called whenever we disconnect from the server
void _gcs_disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    printf("Mavlink server Disconnected\n");
    return;
}

// callback for simple helper when data is ready
void _gcs_helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
    // validate that the data makes sense
    int n_packets, i;
    mavlink_message_t* msg_array = pipe_validate_mavlink_message_t(data, bytes, &n_packets);
    if(msg_array == NULL){
        printf("Msg array is null");
        return;
    }
    MavCamManager* instance = static_cast<MavCamManager*>(context);
    if (instance != nullptr) {
        for(i=0;i<n_packets;i++){
            instance->qgc_message_handler(&(msg_array[i]));
        }
    }
    return;
}

void _video_connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    return;
}

// called whenever we disconnect from the server
void _video_disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    return;
}

void _video_save_cb(__attribute__((unused)) int ch, camera_image_metadata_t meta, __attribute__((unused))char* frame, __attribute__((unused)) void* ctx)
{
    MavCamManager* instance = static_cast<MavCamManager*>(ctx);
    if (instance != nullptr) {

        if(!instance->context.started_recording) return;

        if(meta.format != IMAGE_FORMAT_H264 && meta.format != IMAGE_FORMAT_H265){
            fprintf(stderr, "error, can only record pipes with type h264 or 265\n");
            pipe_client_close(instance->context.vid_pipe_ch);
        }

        // first packet, need to open file
        if(instance->context.encoded_file == NULL){

            // make sure the directory exists, this might have been deleted
            // while we are still running!
            instance->CreateParentDirs(VID_SAVE_DIR);

            // fetch time to use for video filename
            time_t rawtime = time(NULL);
            char time_formatted[100];
            strftime(time_formatted, sizeof(time_formatted)-1, "%Y-%m-%d_%T ", localtime(&rawtime));

            // construct the video filename
            sprintf(instance->context.video_name, "%svideo_%s", VID_SAVE_DIR, time_formatted);
            instance->context.video_name[strlen(instance->context.video_name)-1]=0; // erase the newline from asctime

            if(meta.format == IMAGE_FORMAT_H265){
                strcat(instance->context.video_name, ".h265");
            }else{
                strcat(instance->context.video_name, ".h264");
            }

            // wb: If the file exists, its contents are cleared
            printf("starting new video: %s\n", instance->context.video_name);
            instance->context.encoded_file = fopen(instance->context.video_name, "wb");
            if(! instance->context.encoded_file){
                printf("Cannot open video file!!!! \n");
                pipe_client_close(instance->context.vid_pipe_ch);
            }
            instance->context.started_recording = 1;
        }

        int ret = fwrite(frame, meta.size_bytes, 1, instance->context.encoded_file);

        if(ret!=1){
            perror("failed to write to disk");
            pipe_client_close(instance->context.vid_pipe_ch);
        }
        return;
    }
}

void* qgc_heartbeat_thread_function(__attribute__((unused)) void *context)
{


    MavCamManager* instance = static_cast<MavCamManager*>(context);
    if(instance != nullptr){
        while (1)
        {
            
            mavlink_message_t msg;
            mavlink_msg_heartbeat_pack(
                                instance->mavlink_system_id,
                                instance->context.compid,
                                &msg,
                                MAV_TYPE_CAMERA,
                                MAV_AUTOPILOT_GENERIC,
                                0,
                                0,
                                MAV_STATE_ACTIVE);
            pipe_client_send_control_cmd_bytes(instance->context.to_gcs_pipe_ch, &msg, sizeof(mavlink_message_t));

            // Send heartbeats at 1 Hz rate as per Mavlink protocol
            usleep(1000000);

        }

    }
    return NULL;
}

// camera helper callback whenever a frame arrives
int MavCamManager::_take_snapshot(void)
{
    // lazy way, use the send command tool for now
    char cmd[256];
    sprintf(cmd, "voxl-send-command %s snapshot", context.snapshot_pipe_name);
    system(cmd);
    return 0;
}

void MavCamManager::CreateParentDirs(const char *file_path)
{
  char *dir_path = (char *) malloc(strlen(file_path) + 1);
  const char *next_sep = strchr(file_path, '/');
  while (next_sep != NULL) {
    int dir_path_len = next_sep - file_path;
    memcpy(dir_path, file_path, dir_path_len);
    dir_path[dir_path_len] = '\0';
    mkdir(dir_path, S_IRWXU|S_IRWXG|S_IROTH);
    next_sep = strchr(next_sep + 1, '/');
  }
  free(dir_path);
}


void MavCamManager::_close_video_file(void)
{
    pipe_client_close(context.vid_pipe_ch);
    if(context.encoded_file != NULL){
        fclose(context.encoded_file);
        context.encoded_file = NULL;
        printf("closed the video file %s\n", context.video_name);
    }
    return;
}

uint32_t MavCamManager::get_ms_since_boot()
{
    struct timespec tp;
    clock_gettime(CLOCK_MONOTONIC, &tp);
    uint32_t ms_since_boot = ((tp.tv_sec * 1000) + (tp.tv_nsec / 1000000));
    if (context.debug) printf("Milliseconds since boot %u ms\n", ms_since_boot);
    return ms_since_boot;
}

void MavCamManager::_construct_rtsp_uri(void)
{
    // if we don't have auto ip enabled, just return now having set the default
    if(!context.enable_auto_ip){
        printf("Using default RTSP URI: %s\n", context.rtsp_server_uri);
        return;
    }

    FILE *fp;
    char path[128];
    char output[128];
    char *split_output;
    /* Open the command for reading. */
    fp = popen("/bin/bash voxl-my-ip", "r");
    if (fp == NULL) {
        fprintf(stderr, "WARNING Failed to run voxl-my-ip\n");
        return;
    }

    /* Read the output a line at a time - fgets returns NULL at EOF. */
    while (fgets(path, sizeof(path), fp) != NULL) {
        strncpy(output, path, 127);
    }

    // TODO do more robust checking of the string for valid IP
    int len = strlen(output);
    if(len<7){
        fprintf(stderr, "WARNING voxl-my-ip returned too short of a string: %s\n", output);
        fprintf(stderr, "using default for now: %s\n", context.rtsp_server_uri);
        return;
    }
    for(int i=0; i<(len-1); i++){
        if(output[i] < '.' && output[i] != ' ' && output[i] != '\n'){ // throw out garbage characters
            fprintf(stderr, "WARNING voxl-my-ip did not return a valid IP: %s\n", output);
            fprintf(stderr, "the following character was flagged as invalid: %c (0X%02X)\n", output[i], output[i]);
            fprintf(stderr, "using default for now: %s\n", context.rtsp_server_uri);
            return;
        }
    }


    split_output = strtok(output, " ");

    /* Walk through other tokens. */
    while (split_output != NULL) {
        split_output[strcspn(split_output, "\n")] = 0;
        strcpy(output, split_output);
        split_output = strtok(NULL, ",");
    }

    /* Close the file stream. */
    pclose(fp);

    char url[100] = "rtsp://";
    char url_end[12];
    int port = this->stream_id - 1;
    int max_len = sizeof(url_end); 
    int j = snprintf(url_end, max_len, ":890%d/live", port);
    if(j >= max_len) {
        fprintf(stderr, "Exceeded max buffer length on rtsp stream string");
    }
    strcat(url, output);
    strcat(url, url_end);
    strcpy(context.rtsp_server_uri, url);
    printf("auto-detected RTSP URI: %s\n", context.rtsp_server_uri);
}


void MavCamManager::send_camera_settings()
{

    mavlink_message_t msg;

    // Send the camera settings
    /* @param time_boot_ms [ms] Timestamp (time since system boot).
     * @param mode_id  Camera mode
     * @param zoomLevel  Current zoom level (0.0 to 100.0, NaN if not known)
     * @param focusLevel  Current focus level (0.0 to 100.0, NaN if not known)
     */
    if(context.camera_mode == CAMERA_MODE_VIDEO){
        printf("Sending camera_settings with mode VIDEO to gcs\n");
    } else {
        printf("Sending camera_settings with mode IMAGE to gcs\n");
    }

    mavlink_msg_camera_settings_pack(mavlink_system_id,
                                     context.compid,
                                     &msg,
                                     get_ms_since_boot(),
                                     context.camera_mode,
                                     NAN,
                                     NAN,
                                     0);
    pipe_client_send_control_cmd_bytes(context.to_gcs_pipe_ch, &msg, sizeof(mavlink_message_t));
    
    return;

}


void MavCamManager::send_mavlink_command_ack(uint16_t mav_cmd, uint8_t gcs_sysid, uint8_t gcs_compid)
{
    
    mavlink_message_t msg;

    if (context.debug) printf("Sending command ACK\n");

    mavlink_msg_command_ack_pack(mavlink_system_id,
                                 context.compid,
                                 &msg,
                                 mav_cmd,
                                 MAV_RESULT_ACCEPTED,
                                 0,
                                 0,
                                 gcs_sysid,
                                 gcs_compid);
   pipe_client_send_control_cmd_bytes(context.to_gcs_pipe_ch, &msg, sizeof(mavlink_message_t));
   return;
}


void MavCamManager::qgc_message_handler(mavlink_message_t* msg)
{
    
    mavlink_message_t out_msg;

    if (context.debug) printf("Received msg: ID: %d sys:%d comp:%d\n", msg->msgid, msg->sysid, msg->compid);
    //printf("value of msgid: %i\n", msg->msgid);
    switch (msg->msgid)
    {
    case MAVLINK_MSG_ID_HEARTBEAT:
        //if (context.debug) printf("Heartbeat message at camera\n");
        break;
    case MAVLINK_MSG_ID_PING:
        {
            mavlink_ping_t *ping_data = (mavlink_ping_t*) msg->payload64;
            if( (ping_data->target_system == mavlink_system_id) &&\
                (ping_data->target_component == context.compid)){

                printf("Got a Ping Request from system %d, component %d\n", msg->sysid, msg->compid);
                mavlink_msg_ping_pack(mavlink_system_id, context.compid,
                                      &out_msg,
                                      ping_data->time_usec,
                                      ping_data->seq,
                                      msg->sysid,
                                      msg->compid);
               pipe_client_send_control_cmd_bytes(context.to_gcs_pipe_ch, &out_msg, sizeof(mavlink_message_t));
            }
            else if (context.debug) {
                printf("Got a Ping Response\n");
                printf("\ttime_usec: %" PRIu64 ", seq: %u, target_system: %d, target_component: %d\n",
                       ping_data->time_usec, ping_data->seq, ping_data->target_system,
                       ping_data->target_component);
            }
        }
        break;

    case MAVLINK_MSG_ID_COMMAND_LONG:
        {
            mavlink_command_long_t cmd;
            mavlink_msg_command_long_decode(msg, &cmd);
            //mavlink_command_long_t digicam_ctrl_cmd;

            if (context.debug) printf("Command long message at camera\n");

            mavlink_command_long_t *command_long_data =
                                    (mavlink_command_long_t*) msg->payload64;
            //printf("Value of command long: %i\n", command_long_data->command);
            if (command_long_data->target_system != mavlink_system_id) {
                if(context.debug){
                    fprintf(stderr, "[INFO] Received command for sysid %d, we are %d\n",\
                            command_long_data->target_system,\
                            mavlink_system_id);
                }
                break;
            }
            if(command_long_data->target_component != context.compid){
                if(context.debug){
                    fprintf(stderr, "[INFO] Received command for compid %d, we are %d\n",\
                            command_long_data->target_component,\
                            context.compid);
                }
                break;
            }


            if ((command_long_data->command == MAV_CMD_REQUEST_MESSAGE && command_long_data->param1 == MAVLINK_MSG_ID_CAMERA_INFORMATION) 
                    || command_long_data->command == MAV_CMD_REQUEST_CAMERA_INFORMATION)
            {
                if (context.debug) {
                    printf("Got camera information request\n");

                }

                // Send the Command ACK
                send_mavlink_command_ack(MAV_CMD_REQUEST_MESSAGE, msg->sysid, msg->compid);

                // Send the camera information
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param vendor_name  Name of the camera vendor
                 * @param model_name  Name of the camera model
                 * @param firmware_version  Version of the camera firmware (v << 24 & 0xff = Dev, v << 16 & 0xff = Patch, v << 8 & 0xff = Minor, v & 0xff = Major)
                 * @param focal_length [mm] Focal length
                 * @param sensor_size_h [mm] Image sensor size horizontal
                 * @param sensor_size_v [mm] Image sensor size vertical
                 * @param resolution_h [pix] Horizontal image resolution
                 * @param resolution_v [pix] Vertical image resolution
                 * @param lens_id  Reserved for a lens ID
                 * @param flags  Bitmap of camera capability flags.
                 * @param cam_definition_version  Camera definition version (iteration)
                 * @param cam_definition_uri  Camera definition URI (if any, otherwise only basic functions will be available).
                 *                            HTTP- (http://) and MAVLink FTP- (mavlinkftp://) formatted URIs are allowed (and
                 *                            both must be supported by any GCS that implements the Camera Protocol).
                 */


                uint32_t camera_capabilities =  CAMERA_CAP_FLAGS_CAPTURE_IMAGE | \
                                                CAMERA_CAP_FLAGS_CAPTURE_VIDEO | \
                                                CAMERA_CAP_FLAGS_HAS_MODES | \
                                                CAMERA_CAP_FLAGS_HAS_BASIC_ZOOM | \
                                                CAMERA_CAP_FLAGS_HAS_VIDEO_STREAM;

                mavlink_msg_camera_information_pack(mavlink_system_id,
                                                    context.compid,
                                                    &out_msg,
                                                    get_ms_since_boot(),
                                                    (const uint8_t *) "Unknown",
                                                    (const uint8_t *) context.camera_name,
                                                    1 | (218 << 8),
                                                    100.0,
                                                    10.0,
                                                    10.0,
                                                    640,
                                                    480,
                                                    0,
                                                    camera_capabilities,
                                                    0,
                                                    NULL,
                                                    0,
                                                    0);

                pipe_client_send_control_cmd_bytes(context.to_gcs_pipe_ch, &out_msg, sizeof(mavlink_message_t));
            }

            else if (command_long_data->command == MAV_CMD_SET_CAMERA_MODE)
            {
                if (context.debug)
                {
                    printf("Got camera set mode\n");
                }
                // Send the Command ACK
                send_mavlink_command_ack(MAV_CMD_SET_CAMERA_MODE, msg->sysid, msg->compid);

                if(context.camera_mode == CAMERA_MODE_VIDEO){
                    context.camera_mode = CAMERA_MODE_IMAGE;
                } else {
                    context.camera_mode = CAMERA_MODE_VIDEO;
                }
                send_camera_settings();
            }

            else if ((command_long_data->command == MAV_CMD_REQUEST_MESSAGE && command_long_data->param1 == MAVLINK_MSG_ID_CAMERA_SETTINGS)
                || command_long_data->command == MAV_CMD_REQUEST_CAMERA_SETTINGS)
            {
                if (context.debug)
                {
                    printf("Got camera settings request\n");

                }
                // Send the Command ACK
                send_mavlink_command_ack(MAV_CMD_REQUEST_MESSAGE, msg->sysid, msg->compid);

                // Send the camera settings
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param mode_id  Camera mode
                 * @param zoomLevel  Current zoom level (0.0 to 100.0, NaN if not known)
                 * @param focusLevel  Current focus level (0.0 to 100.0, NaN if not known)
                 */

                mavlink_msg_camera_settings_pack(mavlink_system_id,
                                                 context.compid,
                                                 &out_msg,
                                                 get_ms_since_boot(),
                                                 context.camera_mode,
                                                 NAN,
                                                 NAN,
                                                 0);
                pipe_client_send_control_cmd_bytes(context.to_gcs_pipe_ch, &out_msg, sizeof(mavlink_message_t));

            }
        
            else if ((command_long_data->command == MAV_CMD_REQUEST_MESSAGE && command_long_data->param1 == MAVLINK_MSG_ID_VIDEO_STREAM_INFORMATION) 
                    || command_long_data->command == MAV_CMD_REQUEST_VIDEO_STREAM_INFORMATION)
            {
                if (context.debug)
                {
                    printf("Got video stream information request for stream: %d\n",
                           (int) command_long_data->param2);
                }

                // Send the Command ACK
                send_mavlink_command_ack(MAV_CMD_REQUEST_MESSAGE, msg->sysid, msg->compid);

                // unless manually set, fetch our ip address at the current time
                if(!context.rtsp_manual_uri){
                    _construct_rtsp_uri();
                }

                // Send the video stream information
                /* @param stream_id  Video Stream ID (1 for first, 2 for second, etc.)
                 * @param count  Number of streams available.
                 * @param type  Type of stream.
                 * @param flags  Bitmap of stream status flags.
                 * @param framerate [Hz] Frame rate.
                 * @param resolution_h [pix] Horizontal resolution.
                 * @param resolution_v [pix] Vertical resolution.
                 * @param bitrate [bits/s] Bit rate.
                 * @param rotation [deg] Video image rotation clockwise.
                 * @param hfov [deg] Horizontal Field of view.
                 * @param name  Stream name.
                 * @param uri  Video stream URI (TCP or RTSP URI ground station should connect to) or port number (UDP port ground station should listen to).
                 */
                printf("Sending stream info to gcs: %s\n", context.rtsp_server_uri);
                mavlink_msg_video_stream_information_pack(mavlink_system_id,
                                                          context.compid,
                                                          &out_msg,
                                                          this->stream_id,
                                                          1,
                                                          VIDEO_STREAM_TYPE_RTSP,
                                                          VIDEO_STREAM_STATUS_FLAGS_RUNNING,
                                                          15.0,
                                                          640,
                                                          480,
                                                          1000000,
                                                          0,  // QGC does NOT rotate the image
                                                          60,
                                                          context.camera_name,
                                                          context.rtsp_server_uri,
                                                          1,
                                                          0);
                pipe_client_send_control_cmd_bytes(context.to_gcs_pipe_ch, &out_msg, sizeof(mavlink_message_t));
            }

            else if ((command_long_data->command == MAV_CMD_REQUEST_MESSAGE && command_long_data->param1 == MAVLINK_MSG_ID_VIDEO_STREAM_STATUS) 
                    || command_long_data->command == MAV_CMD_REQUEST_VIDEO_STREAM_STATUS)
            {
                if (context.debug)
                {
                    printf("Got video stream status request for stream: %d\n",
                           (int) command_long_data->param2);
                }

                // Send the Command ACK
                send_mavlink_command_ack(MAV_CMD_REQUEST_MESSAGE, msg->sysid, msg->compid);

                // Send the video stream information
                /* @param stream_id  Video Stream ID (1 for first, 2 for second, etc.)
                 * @param flags  Bitmap of stream status flags
                 * @param framerate [Hz] Frame rate
                 * @param resolution_h [pix] Horizontal resolution
                 * @param resolution_v [pix] Vertical resolution
                 * @param bitrate [bits/s] Bit rate
                 * @param rotation [deg] Video image rotation clockwise
                 * @param hfov [deg] Horizontal Field of view
                 */
                mavlink_msg_video_stream_status_pack(mavlink_system_id, context.compid,
                                                     &out_msg,
                                                     this->stream_id,
                                                     VIDEO_STREAM_STATUS_FLAGS_RUNNING,
                                                     30.0,
                                                     640,
                                                     480,
                                                     1000000,
                                                     0,  // QGC does NOT rotate the image
                                                     60,
                                                     0);
                pipe_client_send_control_cmd_bytes(context.to_gcs_pipe_ch, &out_msg, sizeof(mavlink_message_t));
            }

            else if ((command_long_data->command == MAV_CMD_REQUEST_MESSAGE && command_long_data->param1 == MAVLINK_MSG_ID_STORAGE_INFORMATION)
                || command_long_data->command == MAV_CMD_REQUEST_STORAGE_INFORMATION)
            {

                if (context.debug)
                {
                    printf("Got storage information request. Storage ID: %d\n",
                           (int) command_long_data->param2);

                }

                // Send the Command ACK
                send_mavlink_command_ack(MAV_CMD_REQUEST_MESSAGE, msg->sysid, msg->compid);

                // new extension field to the storage_information packet we don't use
                char name[32];
                memset(name,0,32);

                // Send the storage information
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param storage_id  Storage ID (1 for first, 2 for second, etc.)
                 * @param storage_count  Number of storage devices
                 * @param status  Status of storage
                 * @param total_capacity [MiB] Total capacity. If storage is not ready (STORAGE_STATUS_READY) value will be ignored.
                 * @param used_capacity [MiB] Used capacity. If storage is not ready (STORAGE_STATUS_READY) value will be ignored.
                 * @param available_capacity [MiB] Available storage capacity. If storage is not ready (STORAGE_STATUS_READY) value will be ignored.
                 * @param read_speed [MiB/s] Read speed.
                 * @param write_speed [MiB/s] Write speed.
                 */
                mavlink_msg_storage_information_pack(mavlink_system_id, context.compid,
                                                     &out_msg,
                                                     get_ms_since_boot(),
                                                     1,
                                                     1,
                                                     3,   // STORAGE_STATUS_NOT_SUPPORTED
                                                     0.0,
                                                     100.0,
                                                     0.0,
                                                     0.0,
                                                     0.0,
                                                     0,
                                                     name,
                                                     0);
                pipe_client_send_control_cmd_bytes(context.to_gcs_pipe_ch, &out_msg, sizeof(mavlink_message_t));
            }

            else if ((command_long_data->command == MAV_CMD_REQUEST_MESSAGE && command_long_data->param1 == MAVLINK_MSG_ID_CAMERA_CAPTURE_STATUS)
                || command_long_data->command == MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS)
            {
                if (context.debug) {
                    printf("Got camera capture status request\n");

                }

                // Send the Command ACK
                send_mavlink_command_ack(MAV_CMD_REQUEST_MESSAGE, msg->sysid, msg->compid);

                // Send the camera capture status
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param image_status  Current status of image capturing (0: idle, 1: capture in progress, 2: interval set but idle, 3: interval set and capture in progress)
                 * @param video_status  Current status of video capturing (0: idle, 1: capture in progress)
                 * @param image_interval [s] Image capture interval
                 * @param recording_time_ms [ms] Time since recording started
                 * @param available_capacity [MiB] Available storage capacity.


                 * @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param image_status  Current status of image capturing (0: idle, 1: capture in progress, 2: interval set but idle, 3: interval set and capture in progress
                 * @param video_status  Current status of video capturing (0: idle, 1: capture in progress)
                 * @param image_interval [s] Image capture interval
                 * @param recording_time_ms [ms] Time since recording started
                 * @param available_capacity [MiB] Available storage capacity.
                 * @param image_count  Total number of images captured ('forever', or until reset using MAV_CMD_STORAGE_FORMAT).


                 */
                if(context.camera_mode == CAMERA_MODE_IMAGE){
                    //MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS, MAV_RESULT_ACCEPTED
                    mavlink_msg_camera_capture_status_pack(mavlink_system_id, context.compid,
                                                        &out_msg,
                                                        get_ms_since_boot(),
                                                        0,
                                                        0,
                                                        0.0,
                                                        0,
                                                        0.0,
                                                        0,
                                                        0);
                } else if (context.camera_mode == CAMERA_MODE_VIDEO && context.started_recording == 1){
                    //MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS, MAV_RESULT_ACCEPTED
                    mavlink_msg_camera_capture_status_pack(mavlink_system_id, context.compid,
                                                        &out_msg,
                                                        get_ms_since_boot(),
                                                        0,
                                                        1,
                                                        0.0,
                                                        0,
                                                        0.0,
                                                        0,
                                                        0);
                } else {
                    //MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS, MAV_RESULT_ACCEPTED
                    mavlink_msg_camera_capture_status_pack(mavlink_system_id, context.compid,
                                                        &out_msg,
                                                        get_ms_since_boot(),
                                                        0,
                                                        0,
                                                        0.0,
                                                        0,
                                                        0.0,
                                                        0,
                                                        0);
                }
                pipe_client_send_control_cmd_bytes(context.to_gcs_pipe_ch, &out_msg, sizeof(mavlink_message_t));
            }

            else if (command_long_data->command == MAV_CMD_IMAGE_START_CAPTURE)
            {
                _take_snapshot();

                if (context.debug)
                {
                    printf("Capture Request");
                }
                send_mavlink_command_ack(MAV_CMD_IMAGE_START_CAPTURE, msg->sysid, msg->compid);

                // Send back CAMERA_IMAGE_CAPTURED
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param time_utc [us] Timestamp (time since UNIX epoch) in UTC. 0 for unknown.
                 * @param camera_id  Deprecated/unused. Component IDs are used to differentiate multiple cameras.
                 * @param lat [degE7] Latitude where image was taken
                 * @param lon [degE7] Longitude where capture was taken
                 * @param alt [mm] Altitude (MSL) where image was taken
                 * @param relative_alt [mm] Altitude above ground
                 * @param q  Quaternion of camera orientation (w, x, y, z order, zero-rotation is 1, 0, 0, 0)
                 * @param image_index  Zero based index of this image (i.e. a new image will have index CAMERA_CAPTURE_STATUS.image count -1)
                 * @param capture_result  Boolean indicating success (1) or failure (0) while capturing this image.
                 * @param file_url  URL of image taken. Either local storage or http://foo.jpg if camera provides an HTTP interface.
                 * @return length of the message in bytes (excluding serial stream start sign)
                 */

                float q[4] = {1.0, 0.0, 0.0, 0.0};
                // TODO send back the actual file name
                char fname[MAVLINK_MSG_ID_CAMERA_IMAGE_CAPTURED_LEN] = "modalai-capture.jpg";
                mavlink_msg_camera_image_captured_pack(mavlink_system_id,
                                                       context.compid,
                                                       &out_msg,
                                                       get_ms_since_boot(),
                                                       0,
                                                       0,
                                                       0,
                                                       0,
                                                       0,
                                                       0,
                                                       q,
                                                       0,
                                                       1,
                                                       fname);
                pipe_client_send_control_cmd_bytes(context.to_gcs_pipe_ch, &out_msg, sizeof(mavlink_message_t));
            }
            else if (command_long_data->command == MAV_CMD_IMAGE_STOP_CAPTURE)
            {

                if (context.debug)
                {
                    printf("Calling stop capture");
                }
                send_mavlink_command_ack(MAV_CMD_IMAGE_STOP_CAPTURE, msg->sysid, msg->compid);
            }
            else if (command_long_data->command == MAV_CMD_VIDEO_START_CAPTURE)
            {
                if(context.debug) printf("Got command to start a video!\n");
                // make sure the pipe and video are closed before starting a new one
                pipe_client_close(context.vid_pipe_ch);
                _close_video_file(); // make sure the last one is closed

                if(context.started_recording){
                    printf("QGC, asked us to start a video while we were still recording\n");
                    printf("stopping video and not starting a new one yet\n");
                    context.started_recording = 0;
                    send_mavlink_command_ack(MAV_CMD_VIDEO_STOP_CAPTURE, msg->sysid, msg->compid);
                    _close_video_file();
                    break;
                }

                context.encoded_file = NULL;
                context.started_recording = 1;

                send_mavlink_command_ack(MAV_CMD_VIDEO_START_CAPTURE, msg->sysid, msg->compid);

                pipe_client_set_connect_cb(context.vid_pipe_ch, _video_connect_cb, this);
                pipe_client_set_disconnect_cb(context.vid_pipe_ch, _video_disconnect_cb, this);
                pipe_client_set_camera_helper_cb(context.vid_pipe_ch, _video_save_cb, this);
                pipe_client_open(context.vid_pipe_ch, context.video_pipe_name, CLIENT_NAME, EN_PIPE_CLIENT_CAMERA_HELPER, 0);

            }
            else if (command_long_data->command == MAV_CMD_VIDEO_STOP_CAPTURE){

                if(context.debug) printf("Got command to stop the video!\n");

                context.started_recording = 0;
                send_mavlink_command_ack(MAV_CMD_VIDEO_STOP_CAPTURE, msg->sysid, msg->compid);
                _close_video_file();
            }

            // nothing to reset, just ack the command to stop QGC from freezing
            else if (command_long_data->command == MAV_CMD_RESET_CAMERA_SETTINGS){
                send_mavlink_command_ack(MAV_CMD_RESET_CAMERA_SETTINGS, msg->sysid, msg->compid);
            }
            else 
            {
                if (context.debug) printf("Got unknown command %d\n\n\n", command_long_data->command);
                break;
            }
            if (context.debug) printf("Confirmation number: %d\n", command_long_data->confirmation);
        }

        break;

    default:
        if (context.debug) printf("Unknown message type : %d\n\n\n\n", msg->msgid);
        break;
    }

}

int MavCamManager::qgc_init()
{
    CreateParentDirs(VID_SAVE_DIR);
    CreateParentDirs("/data/snapshots/");

    // Start the thread to receive and process incoming GCS messages.
    //pthread_create(&qgc_thread_id, NULL, qgc_thread_function, NULL);
    pipe_client_set_connect_cb(context.from_gcs_pipe_ch, _gcs_connect_cb, this);
    pipe_client_set_disconnect_cb(context.from_gcs_pipe_ch, _gcs_disconnect_cb, this);
    pipe_client_set_simple_helper_cb(context.from_gcs_pipe_ch, _gcs_helper_cb, this);
    pipe_client_open(context.from_gcs_pipe_ch, FROM_GCS_PIPE_NAME, CLIENT_NAME, \
                                            EN_PIPE_CLIENT_SIMPLE_HELPER, \
                                            MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);

    pipe_client_set_connect_cb(context.to_gcs_pipe_ch, _gcs_connect_cb, this);
    pipe_client_set_disconnect_cb(context.to_gcs_pipe_ch, _gcs_disconnect_cb, this);
    pipe_client_set_simple_helper_cb(context.to_gcs_pipe_ch, _gcs_helper_cb, this);
    pipe_client_open(context.to_gcs_pipe_ch, TO_GCS_PIPE_NAME, CLIENT_NAME, \
                                            EN_PIPE_CLIENT_SIMPLE_HELPER, \
                                            MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
    // Start the thread to send out the periodic heartbeat message.
    pthread_create(&qgc_heartbeat_thread_id, NULL, qgc_heartbeat_thread_function, this);
    return 0;
}

void MavCamManager::qgc_shutdown() {
    if (context.debug) printf("Waiting for the threads to exit\n");
    pthread_cancel(qgc_heartbeat_thread_id);
    pipe_client_close(context.from_gcs_pipe_ch);
    pipe_client_close(context.to_gcs_pipe_ch);
}
