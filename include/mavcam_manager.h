/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef MAV_CAM_MANAGER_H
#define MAV_CAM_MANAGER_H

#include <stdio.h>
#include <stdint.h>

#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <inttypes.h>
#include <sys/stat.h>

#include <c_library_v2/common/mavlink.h>
#include <c_library_v2/development/development.h>

#include <voxl_cutils.h>
#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include "context.h"

#define UDP_READ_BUF_LEN 512
#define MAX_MESSAGE_LENGTH 256

class MavCamManager {
public:

    MavCamManager(){};
    ~MavCamManager(){};

    /**
     * @brief      Initialize the MavCamManager module and start helper threads
     *
     * @param[in]  ctx     A pointer to the application context data structure
     *
     * @return     0 on success, -1 on failure
     */
    int qgc_init();

    /**
     * @brief      Shutdown the MavCamManager module
     */
    void qgc_shutdown();

    int _take_snapshot(void);
    void CreateParentDirs(const char *file_path);
    void _close_video_file(void);
    uint32_t get_ms_since_boot();
    void _construct_rtsp_uri(void);
    void send_camera_settings();
    void send_mavlink_command_ack(uint16_t mav_cmd, uint8_t gcs_sysid, uint8_t gcs_compid);
    void qgc_message_handler(mavlink_message_t* msg);

    context_data context;
    int mavlink_system_id = 1;
    // Public member variable that stores stream ID, very important otherwise QGC rejects multiple streams with same ID
    uint8_t stream_id;

private:

    // Thread definitions
    pthread_t qgc_heartbeat_thread_id;


};

#endif /* MAV_CAM_MANAGER_H */
