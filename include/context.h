/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/**
 * @file context.h
 *
 * This file contains the definition of the main data structure for the
 * application.
 */

#ifndef CONTEXT_H
#define CONTEXT_H

#include <stdint.h>
#include <iostream>

#define PROGRAM_ERROR -1

#define QGC_IP_ADDRESS_MAX_SIZE 64

// Per Mavlink video stream information message
#define RTSP_URI_MAX_SIZE 160

// Per Mavlink camera information message
#define CAMERA_NAME_MAX_SIZE 32

// Maximum camera component ids available in Mavlink
#define MAX_MAVLINK_CAMERAS 6

#define MAX_PATH_LEN 128


#define VID_SAVE_DIR            "/data/video/"
#define VID_EXTENSION           ".h264"
#define VID_RECORD_PIPE_NAME    "hires_large_h264"
#define SNAPSHOT_PIPE_NAME      "hires_snapshot"
#define CLIENT_NAME             "voxl-mavcam-manager"
#define FROM_GCS_PIPE_NAME        "mavlink_from_gcs"
#define TO_GCS_PIPE_NAME        "mavlink_to_gcs"


// Structure to contain all needed information, so we can pass it to callbacks
typedef struct _context_data {

    char rtsp_server_uri[RTSP_URI_MAX_SIZE];
    char camera_name[CAMERA_NAME_MAX_SIZE];
    char video_name[MAX_PATH_LEN];

    char snapshot_pipe_name[CAMERA_NAME_MAX_SIZE];
    char video_pipe_name[CAMERA_NAME_MAX_SIZE];

    char client_name[CAMERA_NAME_MAX_SIZE];

    uint8_t compid;

    uint8_t debug;
    uint8_t frame_debug;
    uint8_t rtsp_manual_uri;

    int camera_mode;
    int grab_image;

    pthread_mutex_t lock;
    int vid_pipe_ch; 
    int from_gcs_pipe_ch;
    int to_gcs_pipe_ch;
    int enable_auto_ip;
    int started_recording;
    int mavlink_sys_id;
    FILE* encoded_file;

} context_data;

#endif // CONTEXT_H
